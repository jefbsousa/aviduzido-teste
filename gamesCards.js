
const fs = require('fs');
const request = require('request');

var getGames = () => {
  return new Promise( (res, rej) => {
    res( JSON.parse(fs.readFileSync('gamesArray.json', 'utf8')) );
  });
};


var CreateMessageData = (games) => {
  return  new Promise( (res, rej) => {

    var messageData = {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: []
        }
      }
    }

    games.games.forEach( (element, index) => {
      if( element.short_name !== 'cf'){
        messageData.attachment.payload.elements[index] = {
          title: element.title,
          subtitle: element.desc,
          image_url: element.img_url,
          buttons: [{
            type: "web_url",
            url: element.url,
            title: "Baixar Jogo"
          }],
        }
      }

    });
    res(messageData);
  });
};


var sendCardGames = () => {

  getGames().then( (games) => {
    return CreateMessageData(games);
  }).then( (messageData) => {
    request({
      url: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {
        access_token: process.env.FB_TOKEN
      },
      method: 'POST',
      json: {
        recipient: {
          id: sender
        },
        message: messageData
        // "sender_action" : "typing_on"
      }
    },
    function(error, response, body) {
      if (error) {
        console.log('Error sending message: ', error);
      } else if (response.body.error) {
        console.log('Error: ', response.body.error);
      }
    });

  }).catch( (errMsg) => {
    console.log(errMsg);
  });

};

module.exports.sendCardGames = sendCardGames;
