
const request = require('request');

var typing_action = (sender) =>{
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: process.env.FB_TOKEN },
    method: 'POST',
    json: {
      "recipient":{
            "id": sender
      },
      "sender_action":"typing_on" // typing_off
    }
  }, function (error, response, body) {
    if (error) {
      console.log(error);
    } else if (response.body.error) {
      console.log('Typing indicator sent');
    }
  });

}

module.exports.typing_action = typing_action;
