# curl -X POST -H "Content-Type: application/json" -d '{
#   "get_started":{
#     "payload":"Começar"
#   }
# }' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAC56IgXBlUBAEks4YeOSZCH8FZAnCCdSmJZBeUk3kZAaYwYiI3wJJaIMbAeaZBdZAahfO0UPYbfOc7ROU5vZCwlq6sQCBdSYfwLAeWXM5JDKFt6wFNiCwzxzt98tXodoFf7uazZC42O8W6ApM8xJEr4ZAxN52GfspfpzAqVX4rEVnQZDZD"
#
# curl -X POST -H "Content-Type: application/json" -d '{
#   "greeting":[
#     {
#       "locale":"default",
#       "text":"Olá {{user_first_name}}, Bem-vindo à página da AVIDO!"
#     }
#   ]
# }'  "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAC56IgXBlUBAEks4YeOSZCH8FZAnCCdSmJZBeUk3kZAaYwYiI3wJJaIMbAeaZBdZAahfO0UPYbfOc7ROU5vZCwlq6sQCBdSYfwLAeWXM5JDKFt6wFNiCwzxzt98tXodoFf7uazZC42O8W6ApM8xJEr4ZAxN52GfspfpzAqVX4rEVnQZDZD"
#
# curl -X POST -H "Content-Type: application/json" -d '{
#   "persistent_menu":[
#        { "locale": "default",
#          "composer_input_disabled": false,
#          "call_to_actions": [
#            {
#           "title":"Sobre a AVIDO",
#           "type":"nested",
#           "call_to_actions":[
#             {
#               "title":"Quem somos",
#               "type":"postback",
#               "payload":"quem somos"
#             },
#             {
#               "title":"Contato",
#               "type":"postback",
#               "payload":"contato da avido"
#             },
#             {
#               "title":"Nossos jogos",
#               "type":"postback",
#               "payload":"games"
#             },
#             {
#               "title":"Localização",
#               "type":"postback",
#               "payload":"localização"
#             }
#
#           ]
#         },
#         {
#           "type":"web_url",
#           "title":"Ir para o site",
#           "url":"http://avi.do"
#         }
#       ]
#     }
#
#   ]
# }' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAC56IgXBlUBAEks4YeOSZCH8FZAnCCdSmJZBeUk3kZAaYwYiI3wJJaIMbAeaZBdZAahfO0UPYbfOc7ROU5vZCwlq6sQCBdSYfwLAeWXM5JDKFt6wFNiCwzxzt98tXodoFf7uazZC42O8W6ApM8xJEr4ZAxN52GfspfpzAqVX4rEVnQZDZD"

#
# curl -X GET "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAC56IgXBlUBAEks4YeOSZCH8FZAnCCdSmJZBeUk3kZAaYwYiI3wJJaIMbAeaZBdZAahfO0UPYbfOc7ROU5vZCwlq6sQCBdSYfwLAeWXM5JDKFt6wFNiCwzxzt98tXodoFf7uazZC42O8W6ApM8xJEr4ZAxN52GfspfpzAqVX4rEVnQZDZD"
#
#
# curl -X DELETE -H "Content-Type: application/json" -d '{
#   "fields":[
#     "persistent_menu"
#   ]
# }' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAC56IgXBlUBAEks4YeOSZCH8FZAnCCdSmJZBeUk3kZAaYwYiI3wJJaIMbAeaZBdZAahfO0UPYbfOc7ROU5vZCwlq6sQCBdSYfwLAeWXM5JDKFt6wFNiCwzxzt98tXodoFf7uazZC42O8W6ApM8xJEr4ZAxN52GfspfpzAqVX4rEVnQZDZD"
#
#
#
# {
#     "recipient": {"id": "132456"},
#     "message": {
#         "attachment": {
#             "type": "template",
#             "payload": {
#                 "template_type": "generic",
#                 "elements": {
#                     "element": {
#                         "title": "Your current location",
#                         "image_url": "https://dal.objectstorage.open.softlayer.com/v1/AUTH_f82bcb88c10b445ab10d35387aaf671b/maps/avido_maps2.png",
#                         "item_url": "https://www.google.com.br/maps/place/%C3%81VIDO/@-23.547091,-46.6478876,16z/data=!4m12!1m6!3m5!1s0x0:0x999bd697549122b!2zw4FWSURP!8m2!3d-23.5469932!4d-46.6454509!3m4!1s0x0:0x999bd697549122b!8m2!3d-23.5469932!4d-46.6454509"
#                     }
#                 }
#             }
#         }
#     }
# }
#
#
# {
#   "recipient":{
#     "id": sender
#   },
#   "message":{
#     "attachment":{
#       "type":"template",
#       "payload":{
#         "template_type":"generic",
#         "elements":[
#            {
#             "title":"Welcome to Peter\'s Hats",
#             "image_url":"https://petersfancybrownhats.com/company_image.png",
#             "subtitle":"We\'ve got the right hat for everyone.",
#             "default_action": {
#               "type": "web_url",
#               "url": "https://peterssendreceiveapp.ngrok.io/view?item=103",
#               "messenger_extensions": true,
#               "webview_height_ratio": "tall",
#               "fallback_url": "https://peterssendreceiveapp.ngrok.io/"
#             },
#             "buttons":[
#               {
#                 "type":"web_url",
#                 "url":"https://petersfancybrownhats.com",
#                 "title":"View Website"
#               },{
#                 "type":"postback",
#                 "title":"Start Chatting",
#                 "payload":"DEVELOPER_DEFINED_PAYLOAD"
#               }
#             ]
#           }
#         ]
#       }
#     }
#   }
# }
#
# #https://maps.googleapis.com/maps/api/staticmap?size=764x400&center=-23.5469883,-46.6476396&zoom=25&markers=-23.5469883,-46.6476396
# # https://www.google.com.br/maps/place/%C3%81VIDO/@-23.547091,-46.6478876,16z/data=!4m12!1m6!3m5!1s0x0:0x999bd697549122b!2zw4FWSURP!8m2!3d-23.5469932!4d-46.6454509!3m4!1s0x0:0x999bd697549122b!8m2!3d-23.5469932!4d-46.6454509
#
#
#
# # https://dal.objectstorage.open.softlayer.com/v1/AUTH_f82bcb88c10b445ab10d35387aaf671b/maps/avido_maps2.png
