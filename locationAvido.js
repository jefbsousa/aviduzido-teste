const request = require('request');

var sendLocation  = (sender) => {

  var messageData = {
    attachment: {
      type: "template",
      payload: {
        template_type: "generic",
        elements: [{
          title: "Localização",
          image_url: "https://dal.objectstorage.open.softlayer.com/v1/AUTH_f82bcb88c10b445ab10d35387aaf671b/maps/avido_maps2.png",
          buttons: [{
            type: "web_url",
            url: "https://www.google.com.br/maps/place/%C3%81VIDO/@-23.547091,-46.6478876,16z/data=!4m12!1m6!3m5!1s0x0:0x999bd697549122b!2zw4FWSURP!8m2!3d-23.5469932!4d-46.6454509!3m4!1s0x0:0x999bd697549122b!8m2!3d-23.5469932!4d-46.6454509",
            title: "Ver no mapa"
          }],
        }]
      }
    }
  }

  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
      access_token: process.env.FB_TOKEN
    },
    method: 'POST',
    json: {
      recipient: {
        id: sender
      },
      message: messageData
      // "sender_action" : "typing_on"
    }
  },
  function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });

}

module.exports.sendLocation = sendLocation;
